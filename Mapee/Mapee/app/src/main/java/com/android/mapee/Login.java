package com.android.mapee;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.lang.reflect.Method;

/**
 * Created by arihant on 28/12/17.
 */

public class Login extends Fragment {

    TextView textView3;
    Button button33l;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.login, container, false);
        textView3 = rootView.findViewById(R.id.can);
        button33l = rootView.findViewById(R.id.btn_btn);
        String data = getArguments().getString("key_value");
        textView3.setText(data);
        button33l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Calender_Activty.class));
            }
        });
        return rootView;
    }
    //***************************************************************************************************************
}
