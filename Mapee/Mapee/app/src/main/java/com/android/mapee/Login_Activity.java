package com.android.mapee;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.mapee.Activity.Admin_Activity;
import com.android.mapee.Activity.HouseOwner_Add_Item;
import com.android.mapee.Activity.Sign_Up_Activity;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class Login_Activity extends AppCompatActivity {

    EditText userMobile, userPassword;
    Button mLogin_button, mUserNotMember, admin_Login;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
        userMobile = findViewById(R.id.username);
        userPassword = findViewById(R.id.password);
        mLogin_button = findViewById(R.id.loginbtn);
        admin_Login = findViewById(R.id.btn_admin);
        mUserNotMember = findViewById(R.id.notmember);

        admin_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login_Activity.this, Admin_Activity.class));
                finish();

            }
        });
        ;

        mUserNotMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login_Activity.this, MainActivity.class));

            }
        });
        //****************** API Working Started*******************************

        mLogin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String muserMobile = userMobile.getText().toString();
                String muserPassword = userPassword.getText().toString();
                if (!(muserMobile.isEmpty() || muserPassword.isEmpty())) {
                    userMobile.setText("");
                    userPassword.setText("");
                    url = "http://18.216.51.196:8080/arbitorNetwork/user/login?mobileOrMapeeId=" + muserMobile + "&password=" + muserPassword + "";
                    ApiHitCall();
                } else {
                    Toast.makeText(Login_Activity.this, "Fill Details !!", Toast.LENGTH_SHORT).show();
                }

            }
        });
//*************************************API Working Close *******************************
    }

    public void ApiHitCall() {
        final RequestQueue requestQueue = Volley.newRequestQueue(Login_Activity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("checkresponce", "" + response);
                if (response.contains("Entity Retrieved Successfully")) {
                    startActivity(new Intent(Login_Activity.this, HouseOwner_Add_Item.class));

                } else {
                    final AlertDialog.Builder dialog = new AlertDialog.Builder(Login_Activity.this);
                    // dialog.show();
                    dialog.setTitle("");
                    dialog.setMessage("Please SignUp Now !!\n ");
                    dialog.setPositiveButton("SignUp", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(Login_Activity.this, MainActivity.class));
                            finish();

                        }
                    });
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {


                        }
                    });
                    // dialog.setNegativeButton()
                    dialog.setCancelable(true);
                    dialog.create().show();

                    // Toast.makeText(Login_Activity.this, "Please SignUp !! ", Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AlertDialog.Builder dialog = new AlertDialog.Builder(Login_Activity.this);
        // dialog.show();
        dialog.setTitle("");
        dialog.setCancelable(true);
        dialog.setMessage("Are You Exit\n ");
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // startActivity(new Intent(Login_Activity.this, Login_Activity.class));
                finish();

            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        // dialog.setNegativeButton()

        dialog.create().show();


    }

}
