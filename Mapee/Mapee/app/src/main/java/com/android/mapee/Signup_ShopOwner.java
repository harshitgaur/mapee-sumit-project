package com.android.mapee;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;

import static com.android.mapee.R.layout.support_simple_spinner_dropdown_item;

/**
 * Created by arihant on 28/12/17.
 */

public class Signup_ShopOwner extends Fragment {

    CalendarView mCalendarView;

    String[] Choose_Country = {"India"};
    EditText name, email, mobileno, ulternativemobileno,
            street, pincode, aadharcardno,
            password, MapeeId;
    TextView dob;
    Button button, calender_btn;
    private Spinner State_spinner;
    private Spinner City_spinner;
    boolean datachecking = false;
  /*  private ArrayAdapter<State> stateArrayAdapter;
    private ArrayAdapter<City> cityArrayAdapter;
    private ArrayList<State> states;
    private ArrayList<City> cities;*/
    private String Data;
    private String userType;
    private String DataCity;
    String country;
    Spinner userTypeSpinner;
    private Spinner Country_Spinner;
    TextView textView_Shopowner;
    String ShopOwner_text;

   /* @Nullable
    @Override*/

    /*public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.signup, container, false);

        //textView_Shopowner = rootView.findViewById(R.id.shopowner_text);
        name = rootView.findViewById(R.id.signup_name);
        email = rootView.findViewById(R.id.signup_email);
        dob = rootView.findViewById(R.id.signup_dob);
        mobileno = rootView.findViewById(R.id.signup_mobilenumber);
        ulternativemobileno = rootView.findViewById(R.id.signup_alternateMobileNo);
        street = rootView.findViewById(R.id.signup_street);
        pincode = rootView.findViewById(R.id.signup_pincode);
        aadharcardno = rootView.findViewById(R.id.signup_aadharno);
        button = rootView.findViewById(R.id.signup_btn);
        calender_btn = rootView.findViewById(R.id.calender_btn);
        calender_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.calender);
                mCalendarView = dialog.findViewById(R.id.calender1);
                Button dismiss = dialog.findViewById(R.id.dismiss);
                mCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {
                        String date = year + "-" + month + 1 + "-" + dayOfMonth;
                        Log.v("dateyear", "" + date);
                        dob.setText(date);

                    }
                });
                dismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();


                //startActivity(new Intent(getActivity(), DatePicker_Activity.class));

            }
        });
        password = rootView.findViewById(R.id.signup_password);
        MapeeId = rootView.findViewById(R.id.signup_mapeeid);

        State_spinner = rootView.findViewById(R.id.spinner_state);
        City_spinner = rootView.findViewById(R.id.spinner_city);
        Country_Spinner = rootView.findViewById(R.id.spinner_country);


        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, Choose_Country);
        Country_Spinner.setAdapter(stringArrayAdapter);
        Country_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                country = String.valueOf(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        initializeUI();


        button.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View view) {


                                          try {
                                              checkStatus();
                                          } catch (JSONException e) {
                                              e.printStackTrace();
                                          }
                                      }


                                      public void checkStatus() throws JSONException {

                                          if (!(name.getText().toString().isEmpty() || email.getText().toString().isEmpty() || dob.getText().toString().isEmpty() || mobileno.getText().toString().isEmpty()
                                                  || ulternativemobileno.getText().toString().isEmpty() || street.getText().toString().isEmpty() || pincode.getText().toString().isEmpty() ||
                                                  aadharcardno.getText().toString().isEmpty() || password.getText().toString().isEmpty() || MapeeId.getText().toString().isEmpty())) {

/*//**************************************** Validation Api Working ****************************
                                              StringRequest strreq = new StringRequest(Request.Method.GET,
                                                      "http://18.216.51.196:8080/arbitorNetwork/user/checkUserExist?email=" + email.getText().toString() + "&mobile=" + mobileno.getText().toString() + "&mapeeId=" + MapeeId.getText().toString(),

                                                      new Response.Listener<String>() {
                                                          @RequiresApi(api = Build.VERSION_CODES.M)
                                                          @Override
                                                          public void onResponse(String Response) {
                                                              // Toast.makeText(getActivity(), Response, Toast.LENGTH_SHORT).show();
                                                              if (Response.contains("User not exist")) {
/*//************************************************** Sign Up Api Working ***********************************************************

                                                                  try {
                                                                      RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

                                                                      //"http://18.216.51.196:8080/arbitorNetwork/user/";
                                                                      JSONObject jsonBody = new JSONObject();
                                                                      String URL = "http://18.216.51.196:8080/arbitorNetwork/user/";
                                                                      final String dateDob = dob.getText().toString() + "T" + "00:00:00";
                                                                      jsonBody.put("name", name.getText().toString());
                                                                      jsonBody.put("mobileNo", mobileno.getText().toString());
                                                                      jsonBody.put("email", email.getText().toString());
                                                                      jsonBody.put("dob", dateDob);
                                                                      jsonBody.put("alternateMobileNo", ulternativemobileno.getText().toString());
                                                                      jsonBody.put("street", street.getText().toString());
                                                                      jsonBody.put("pinCode", pincode.getText().toString());
                                                                      jsonBody.put("aadharNo", aadharcardno.getText().toString());
                                                                      jsonBody.put("State", Data);
                                                                      jsonBody.put("City", DataCity);
                                                                      jsonBody.put("userType", "SHOP_OWNER");
                                                                      jsonBody.put("status", "CREATED");
                                                                      jsonBody.put("Country", country);
                                                                      jsonBody.put("password", password.getText().toString());
                                                                      jsonBody.put("mapeeid", MapeeId.getText().toString());

                                                                      final String requestBody = jsonBody.toString();

                                                                      name.setText("");
                                                                      mobileno.setText("");
                                                                      email.setText("");
                                                                      dob.setText("");
                                                                      ulternativemobileno.setText("");
                                                                      street.setText("");
                                                                      pincode.setText("");
                                                                      aadharcardno.setText("");
                                                                      password.setText("");
                                                                      MapeeId.setText("");

                                                                      //Toast.makeText(getActivity(), "Successfully ! ", Toast.LENGTH_SHORT).show();
                                                                      StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                                                                          @Override
                                                                          public void onResponse(String response) {
                                                                              Log.v("VOLLEY", "" + response);

                                                                              AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                                                                              // dialog.show();
                                                                              dialog.setTitle("");
                                                                              dialog.setMessage("Your SignUp is Successfully\n ");
                                                                              dialog.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                                                                                  @Override
                                                                                  public void onClick(DialogInterface dialogInterface, int i) {
                                                                                      startActivity(new Intent(getActivity(), Login_Activity.class));

                                                                                  }
                                                                              });
                                                                              dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                                                  @Override
                                                                                  public void onClick(DialogInterface dialogInterface, int i) {

                                                                                  }
                                                                              });
                                                                              // dialog.setNegativeButton()
                                                                              dialog.setCancelable(true);
                                                                              dialog.create().show();


                                                                              Toast.makeText(getActivity(), "Successfully Saved !!", Toast.LENGTH_SHORT).show();
                                                                              // getFragmentManager().beginTransaction().replace(R.id.ltn_contaniner, new Login()).commit();

                                                                          }
                                                                      }, new Response.ErrorListener() {
                                                                          @Override
                                                                          public void onErrorResponse(VolleyError error) {
                                                                              Log.v("VOLLEY", error.toString());
                                                                              Toast.makeText(getActivity(), "" + error.toString(), Toast.LENGTH_SHORT).show();
                                                                          }
                                                                      })

                                                                      {
                                                                          @Override
                                                                          public String getBodyContentType() {
                                                                              return "application/json; charset=utf-8";

                                                                          }


                                                                          @Override
                                                                          protected Map<String, String> getParams() throws AuthFailureError {
                                                                              Map<String, String> jsonBody = new HashMap<String, String>();
                                                                              jsonBody.put("name", name.getText().toString());
                                                                              jsonBody.put("mobileNo", mobileno.getText().toString());
                                                                              jsonBody.put("email", email.getText().toString());
                                                                              jsonBody.put("dob", dateDob);
                                                                              jsonBody.put("status", "CREATED");
                                                                              jsonBody.put("password", password.getText().toString());
                                                                              jsonBody.put("userType", "SHOP_OWNER");
                                                                              jsonBody.put("alternateMobileNo", ulternativemobileno.getText().toString());
                                                                              jsonBody.put("street", street.getText().toString());
                                                                              jsonBody.put("pinCode", pincode.getText().toString());
                                                                              jsonBody.put("aadharNo", aadharcardno.getText().toString());
                                                                              jsonBody.put("State", Data);
                                                                              jsonBody.put("City", DataCity);
                                                                              jsonBody.put("Country", country);
                                                                              jsonBody.put("mapeeid", MapeeId.getText().toString());
                                                                              //Toast.makeText(getActivity(), "Successfully", Toast.LENGTH_SHORT).show();


                                                                              return super.getParams();
                                                                          }


                                                                          @Override
                                                                          public byte[] getBody() throws AuthFailureError {
                                                                              try {
                                                                                  return requestBody == null ? null : requestBody.getBytes("utf-8");

                                                                              } catch (UnsupportedEncodingException uee) {
                                                                                  VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                                                                  return null;
                                                                              }
                                                                          }

                                                                          private final Response.Listener<String> onPostsLoaded = new Response.Listener<String>() {
                                                                              @Override
                                                                              public void onResponse(String response) {
                                                                                  if (response.contentEquals(response.toString())) {
                                                                                      Toast.makeText(getContext(), "toast", Toast.LENGTH_LONG).show();
                                                                                      openProfile();

                                                                                  } else {
                                                                                      Toast.makeText(getContext(), response, Toast.LENGTH_LONG).show();
                                                                                  }

                                                                              }
                                                                          };


                                                                      };

                                                                      requestQueue.add(stringRequest);
                                                                  } catch (JSONException e) {
                                                                      e.printStackTrace();
                                                                  }


/*//***************************************************************************************************************


                                                              } else {
                                                                  Toast.makeText(getActivity(), "Email & Number is Exit", Toast.LENGTH_LONG).show();
                                                              }
                                                          }
                                                      }, new Response.ErrorListener() {
                                                  @Override
                                                  public void onErrorResponse(VolleyError e) {
                                                      e.printStackTrace();
                                                  }
                                              });
                                              RequestQueue rq = Volley.newRequestQueue(getActivity());
                                              rq.add(strreq);
                                          } else {

                                              Toast.makeText(getActivity(), "Fill Details..", Toast.LENGTH_SHORT).show();
                                          }

                                      }


                                  }


        );

        return rootView;


    }*/


   /* private void openProfile() {
        Intent intent = new Intent(getActivity(), Login.class);
        startActivity(intent);
    }

    private void initializeUI() {


//        countries = new ArrayList<>();
        states = new ArrayList<>();
        cities = new ArrayList<>();

        createLists();

//        countryArrayAdapter = new ArrayAdapter<Country>(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, countries);
        *//*countryArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        country_Spinner.setAdapter(countryArrayAdapter);*//*

        stateArrayAdapter = new ArrayAdapter<State>(getActivity(), support_simple_spinner_dropdown_item, states);
        stateArrayAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
        State_spinner.setAdapter(stateArrayAdapter);

        cityArrayAdapter = new ArrayAdapter<City>(getActivity(), support_simple_spinner_dropdown_item, cities);
        cityArrayAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
        City_spinner.setAdapter(cityArrayAdapter);

       *//* country_Spinner.setOnItemSelectedListener(country_listener);*//*
        State_spinner.setOnItemSelectedListener(state_listener);
        City_spinner.setOnItemSelectedListener(city_listener);
    }


    private AdapterView.OnItemSelectedListener state_listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Data = String.valueOf(position);
            if (position > 0) {

                final State state = (State) State_spinner.getItemAtPosition(position);
                Log.d("SpinnerCountry", "onItemSelected: state: " + state.getStateID());
                ArrayList<City> tempCities = new ArrayList<>();

                State firstState = new State(0, "Choose a State");
                tempCities.add(new City(0, firstState, "Choose a City"));

                for (City singleCity : cities) {
                    if (singleCity.getState().getStateID() == state.getStateID()) {
                        tempCities.add(singleCity);
                    }
                }

                cityArrayAdapter = new ArrayAdapter<City>(getActivity(), support_simple_spinner_dropdown_item, tempCities);
                cityArrayAdapter.setDropDownViewResource(support_simple_spinner_dropdown_item);
                City_spinner.setAdapter(cityArrayAdapter);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private AdapterView.OnItemSelectedListener city_listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            DataCity = String.valueOf(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    private void createLists() {
      *//*  Country country0 = new Country(0, "Choose a Country");
        Country country1 = new Country(1, "Country1");
        Country country2 = new Country(2, "Country2");

        countries.add(new Country(0, "Choose a Country"));
        countries.add(new Country(1, "Country1"));
        countries.add(new Country(2, "Country2"));
*//*
        State state0 = new State(0, "Choose a Country");
        State state1 = new State(1, "Andhra Pradesh (AP)");
        State state2 = new State(2, "Arunachal Pradesh (AR)");
        State state3 = new State(3, "Assam (AS)");
        State state4 = new State(4, "Bihar (BR)");
        State state5 = new State(5, "Chandigarh");
        State state6 = new State(6, "Chhattisgarh (CG)");
        State state7 = new State(7, "Dadra and Nagar Haveli (DN)");
        State state8 = new State(8, "Daman and Diu (DD)");
        State state9 = new State(9, "Delhi (DL)");
        State state10 = new State(10, "Goa (GA)");
        State state11 = new State(11, "Gujarat (GJ)");
        State state12 = new State(12, "Haryana (HR)");
        State state13 = new State(13, "Himachal Pradesh (HP)");
        State state14 = new State(14, "Jammu and Kashmir (JK)");
        State state15 = new State(15, "Jharkhand (JH)");
        State state16 = new State(16, "Karnataka (KA)");
        State state17 = new State(17, "Kerala (KL)");
        State state18 = new State(18, "Madhya Pradesh (MP)");
        State state19 = new State(19, "Maharashtra (MH)");
        State state20 = new State(20, "Manipur (MN)");
        State state21 = new State(21, "Meghalaya (ML)");
        State state22 = new State(22, "Mizoram (MZ)");
        State state23 = new State(23, "Nagaland (NL)");
        State state24 = new State(24, "Orissa (OR)");
        State state25 = new State(25, "Pondicherry (PY)");
        State state26 = new State(26, "Punjab (PB)");
        State state27 = new State(27, "Rajasthan (RJ)");
        State state28 = new State(28, "Sikkim (SK)");
        State state29 = new State(29, "Tamil Nadu (TN)");
        State state30 = new State(30, "Tripura (TR)");
        State state31 = new State(31, "Uttar Pradesh (UP)");
        State state32 = new State(32, "Uttarakhand (UK)");
        State state33 = new State(33, "West Bengal (WB)");


        states.add(state0);
        states.add(state1);
        states.add(state2);
        states.add(state3);
        states.add(state4);
        states.add(state5);
        states.add(state6);
        states.add(state7);
        states.add(state8);
        states.add(state9);
        states.add(state10);
        states.add(state11);
        states.add(state12);
        states.add(state13);
        states.add(state14);
        states.add(state15);
        states.add(state16);
        states.add(state17);
        states.add(state18);
        states.add(state19);
        states.add(state20);
        states.add(state21);
        states.add(state22);
        states.add(state23);
        states.add(state24);
        states.add(state25);
        states.add(state26);
        states.add(state27);
        states.add(state28);
        states.add(state29);
        states.add(state30);
        states.add(state31);
        states.add(state32);
        states.add(state33);

        cities.add(new City(0, state0, "Choose a City"));

        cities.add(new City(1, state1, " Adilabad"));
        cities.add(new City(2, state1, "Anantapur"));
        cities.add(new City(3, state1, "Chittoor"));
        cities.add(new City(4, state1, "Kakinada"));
        cities.add(new City(5, state1, "Guntur"));
        cities.add(new City(6, state1, "Hyderabad"));
        cities.add(new City(7, state1, "Karimnagar"));
        cities.add(new City(8, state1, "Khammam"));
        cities.add(new City(9, state1, "Krishna"));
        cities.add(new City(10, state1, "Kurnool"));
        cities.add(new City(11, state1, "Mahbubnagar"));
        cities.add(new City(11, state1, "Medak"));
        cities.add(new City(12, state1, "Nalgonda"));
        cities.add(new City(13, state1, "Nizamabad"));
        cities.add(new City(14, state1, "Ongole"));
        cities.add(new City(15, state1, "Hyderabad"));
        cities.add(new City(16, state1, "Srikakulam"));
        cities.add(new City(17, state1, "Nellore"));
        cities.add(new City(18, state1, "Visakhapatnam"));
        cities.add(new City(19, state1, "Vizianagaram"));
        cities.add(new City(20, state1, "Warangal"));
        cities.add(new City(21, state1, "Eluru"));
        cities.add(new City(22, state1, "Kadapa"));


        cities.add(new City(23, state2, "Anjaw"));
        cities.add(new City(24, state2, "Changlang"));
        cities.add(new City(25, state2, "East Siang"));
        cities.add(new City(26, state2, "Kurung Kumey"));
        cities.add(new City(27, state2, "Lohit"));
        cities.add(new City(28, state2, "Lower Dibang Valley"));
        cities.add(new City(29, state2, "Lower Subansiri"));
        cities.add(new City(30, state2, "Papum Pare"));
        cities.add(new City(31, state2, "Tawang"));
        cities.add(new City(32, state2, "Tirap"));
        cities.add(new City(33, state2, "Dibang Valley"));
        cities.add(new City(34, state2, "Upper Siang"));
        cities.add(new City(35, state2, "Upper Subansiri"));
        cities.add(new City(36, state2, "West Kameng"));
        cities.add(new City(37, state2, "West Siang"));

        cities.add(new City(38, state3, "Baksa"));
        cities.add(new City(39, state3, "Barpeta"));
        cities.add(new City(40, state3, "Bongaigaon"));
        cities.add(new City(41, state3, "Cachar"));
        cities.add(new City(42, state3, "Chirang"));
        cities.add(new City(43, state3, "Darrang"));
        cities.add(new City(44, state3, "Dhemaji"));
        cities.add(new City(45, state3, "Dima Hasao"));
        cities.add(new City(46, state3, "Dhubri"));
        cities.add(new City(47, state3, "Dibrugarh"));
        cities.add(new City(48, state3, "Goalpara"));
        cities.add(new City(49, state3, "Golaghat"));
        cities.add(new City(50, state3, "Hailakandi"));
        cities.add(new City(51, state3, "Jorhat"));
        cities.add(new City(52, state3, "Kamrup"));
        cities.add(new City(53, state3, "Kamrup Metropolitan"));
        cities.add(new City(54, state3, "Karbi Anglong"));
        cities.add(new City(55, state3, "Karimganj"));
        cities.add(new City(56, state3, "Kokrajhar"));
        cities.add(new City(57, state3, "Lakhimpur"));
        cities.add(new City(58, state3, "Marigaon"));
        cities.add(new City(59, state3, "Nagaon"));
        cities.add(new City(60, state3, "Nalbari"));
        cities.add(new City(61, state3, "Sibsagar"));
        cities.add(new City(62, state3, "Sonitpur"));
        cities.add(new City(63, state3, "Tinsukia"));
        cities.add(new City(64, state3, "Udalguri"));


        cities.add(new City(65, state4, "Araria"));
        cities.add(new City(66, state4, "Arwal"));
        cities.add(new City(67, state4, "Aurangabad"));
        cities.add(new City(68, state4, "Banka"));
        cities.add(new City(69, state4, "Begusarai"));
        cities.add(new City(70, state4, "Bhagalpur"));
        cities.add(new City(71, state4, "Bhojpur"));
        cities.add(new City(72, state4, "Buxar"));
        cities.add(new City(73, state4, "Darbhanga"));
        cities.add(new City(74, state4, "East Champaran"));
        cities.add(new City(75, state4, "Gaya"));
        cities.add(new City(75, state4, "Gopalganj"));
        cities.add(new City(75, state4, "Jamui"));
        cities.add(new City(75, state4, "Jehanabad"));
        cities.add(new City(75, state4, "Kaimur"));
        cities.add(new City(76, state4, "Katihar"));
        cities.add(new City(77, state4, "Khagaria"));
        cities.add(new City(78, state4, "Kishanganj"));
        cities.add(new City(79, state4, "Lakhisarai"));
        cities.add(new City(80, state4, "Madhepura"));
        cities.add(new City(81, state4, "Madhubani"));
        cities.add(new City(82, state4, "Munger"));
        cities.add(new City(83, state4, "Muzaffarpur"));
        cities.add(new City(84, state4, "Nalanda"));
        cities.add(new City(85, state4, "Nawada"));
        cities.add(new City(86, state4, "Patna"));
        cities.add(new City(87, state4, "Purnia"));
        cities.add(new City(88, state4, "Rohtas"));
        cities.add(new City(89, state4, "Saharsa"));
        cities.add(new City(90, state4, "Samastipur"));
        cities.add(new City(91, state4, "Saran"));
        cities.add(new City(92, state4, "Sheikhpura"));
        cities.add(new City(93, state4, "Sheohar"));
        cities.add(new City(94, state4, "Sitamarhi"));
        cities.add(new City(95, state4, "Siwan"));
        cities.add(new City(96, state4, "Supaul"));
        cities.add(new City(97, state4, "Vaishali"));
        cities.add(new City(98, state4, "West Champaran"));


        cities.add(new City(99, state5, "Chandigarh"));

        cities.add(new City(100, state6, "Bastar"));
        cities.add(new City(101, state6, "Bijapur"));
        cities.add(new City(102, state6, "Bilaspur"));
        cities.add(new City(103, state6, "Dantewada"));
        cities.add(new City(104, state6, "Dhamtari"));
        cities.add(new City(105, state6, "Durg"));
        cities.add(new City(106, state6, "Jashpur"));
        cities.add(new City(107, state6, "Janjgir-Champa"));
        cities.add(new City(108, state6, "Korba"));
        cities.add(new City(109, state6, "Koriya"));
        cities.add(new City(110, state6, "Kanker"));
        cities.add(new City(111, state6, "Kabirdham (Kawardha)"));
        cities.add(new City(112, state6, "Mahasamund"));
        cities.add(new City(113, state6, "Narayanpur"));
        cities.add(new City(114, state6, "Raigarh"));
        cities.add(new City(115, state6, "Rajnandgaon"));
        cities.add(new City(116, state6, "Raipur"));
        cities.add(new City(117, state6, "Surguja"));

        cities.add(new City(118, state7, "Dadra and Nagar Haveli"));

        cities.add(new City(119, state8, "Daman"));
        cities.add(new City(120, state8, "Diu"));


        cities.add(new City(121, state9, "Central Delhi"));
        cities.add(new City(122, state9, "East Delhi"));
        cities.add(new City(123, state9, "New  Delhi"));
        cities.add(new City(124, state9, "North  Delhi"));
        cities.add(new City(125, state9, "North East Delhi"));
        cities.add(new City(126, state9, "North West Delhi"));
        cities.add(new City(127, state9, "South Delhi"));
        cities.add(new City(128, state9, "South West Delhi"));
        cities.add(new City(129, state9, "West Delhi"));


        cities.add(new City(130, state10, "North Goa"));
        cities.add(new City(131, state10, "South Goa"));

        cities.add(new City(132, state11, "Ahmedabad"));
        cities.add(new City(133, state11, "Amreli district"));
        cities.add(new City(134, state11, "Anand"));
        cities.add(new City(135, state11, "Banaskantha"));
        cities.add(new City(136, state11, "Bharuch"));
        cities.add(new City(137, state11, "Bhavnagar"));
        cities.add(new City(138, state11, "Dahod"));
        cities.add(new City(139, state11, "The Dangs"));
        cities.add(new City(140, state11, "Gandhinagar"));
        cities.add(new City(141, state11, "Jamnagar"));
        cities.add(new City(142, state11, "Junagadh"));
        cities.add(new City(143, state11, "Kutch"));
        cities.add(new City(144, state11, "Kheda"));
        cities.add(new City(145, state11, "Mehsana"));
        cities.add(new City(146, state11, "Narmada"));
        cities.add(new City(147, state11, "Navsari"));
        cities.add(new City(148, state11, "Patan"));
        cities.add(new City(149, state11, "Panchmahal"));
        cities.add(new City(150, state11, "Porbandar"));
        cities.add(new City(151, state11, "Rajkot"));
        cities.add(new City(152, state11, "Sabarkantha"));
        cities.add(new City(153, state11, "Surendranagar"));
        cities.add(new City(154, state11, "Surat"));
        cities.add(new City(155, state11, "Vyara"));
        cities.add(new City(156, state11, "Vadodara"));
        cities.add(new City(157, state11, "Valsad"));

        cities.add(new City(158, state12, "Ambala"));
        cities.add(new City(159, state12, "Bhiwani"));
        cities.add(new City(160, state12, "Faridabad"));
        cities.add(new City(161, state12, "Fatehabad"));
        cities.add(new City(162, state12, "Gurgaon"));
        cities.add(new City(163, state12, "Hissar"));
        cities.add(new City(164, state12, "Jhajjar"));
        cities.add(new City(165, state12, "Jind"));
        cities.add(new City(166, state12, "Karnal"));
        cities.add(new City(167, state12, "Kaithal"));
        cities.add(new City(168, state12, "Kurukshetra"));
        cities.add(new City(169, state12, "Mahendragarh"));
        cities.add(new City(170, state12, "Mewat"));
        cities.add(new City(171, state12, "Palwal"));
        cities.add(new City(172, state12, "Panchkula"));
        cities.add(new City(173, state12, "Panipat"));
        cities.add(new City(174, state12, "Rewari"));
        cities.add(new City(175, state12, "Rohtak"));
        cities.add(new City(176, state12, "Sirsa"));
        cities.add(new City(177, state12, "Sonipat"));
        cities.add(new City(177, state12, "Yamuna Nagar"));


    }
   *//* private class Country implements Comparable<Country> {

        private int countryID;
        private String countryName;


        public Country(int countryID, String countryName) {
            this.countryID = countryID;
            this.countryName = countryName;
        }

        public int getCountryID() {
            return countryID;
        }

        public String getCountryName() {
            return countryName;
        }

        @Override
        public String toString() {
            return countryName;
        }


        @Override
        public int compareTo(Country another) {
            return this.getCountryID() - another.getCountryID();//ascending order
//            return another.getCountryID()-this.getCountryID();//descending  order
        }
    }*//*

    private class State implements Comparable<State> {

        private int stateID;

        private String stateName;

        public State(int stateID, String stateName) {
            this.stateID = stateID;

            this.stateName = stateName;
        }

        public int getStateID() {
            return stateID;
        }


        public String getStateName() {
            return stateName;
        }

        @Override
        public String toString() {
            return stateName;
        }

        @Override
        public int compareTo(State another) {
            return this.getStateID() - another.getStateID();//ascending order
//            return another.getStateID()-this.getStateID();//descending order
        }
    }

    private class City implements Comparable<City> {

        private int cityID;

        private State state;
        private String cityName;

        public City(int cityID, State state, String cityName) {
            this.cityID = cityID;

            this.state = state;
            this.cityName = cityName;
        }

        public int getCityID() {
            return cityID;
        }


        public State getState() {
            return state;
        }

        public String getCityName() {
            return cityName;
        }

        @Override
        public String toString() {
            return cityName;
        }

        @Override
        public int compareTo(City another) {
            return this.cityID - another.getCityID();//ascending order
//            return another.getCityID() - this.cityID;//descending order
        }
    }
*/

}
