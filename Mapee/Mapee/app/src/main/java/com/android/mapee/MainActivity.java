package com.android.mapee;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.mapee.Activity.Admin_Activity;
import com.android.mapee.Activity.SignUp_House_Owner;
import com.android.mapee.Activity.SignUp_User_Guest_Activity;
import com.android.mapee.Activity.Sign_Up_Activity;

public class MainActivity extends AppCompatActivity {

    String[] usreArray = {"Shop Owner", "House Owner", "Guest"};
    Button mLogin, user_btn, house_btn, shop_btn;
    Spinner userTypeSpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLogin = findViewById(R.id.mlogin);
        user_btn = findViewById(R.id.user);
        house_btn = findViewById(R.id.house_owner);
        shop_btn = findViewById(R.id.shop_owner);

        user_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SignUp_User_Guest_Activity.class));
            }
        });

        shop_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Sign_Up_Activity.class));
            }
        });
        house_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SignUp_House_Owner.class));
            }
        });
/*
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this, R.layout.support_simple_spinner_dropdown_item, usreArray);
        userTypeSpinner.setAdapter(arrayAdapter);
        userTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {


               *//* if (position == 0) {
                    startActivity(new Intent(MainActivity.this, Sign_Up_Activity.class));
                } else if (position == 1) {
                    startActivity(new Intent(MainActivity.this, SignUp_House_Owner.class));
                } else if (position == 1) {
                    startActivity(new Intent(MainActivity.this, SignUp_User_Guest_Activity.class));

                } else
                    Toast.makeText(MainActivity.this, "choose right Area", Toast.LENGTH_SHORT).show();
                //Toast.makeText(getActivity(), ""+pos, Toast.LENGTH_SHORT).show();
*//*

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                // dialog.show();
                dialog.setTitle("");
                dialog.setMessage("Choose Your Login\n ");
                dialog.setPositiveButton("User", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(MainActivity.this, Login_Activity.class));
                        finish();


                    }

                });
                dialog.setNegativeButton("Admin", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(MainActivity.this, Admin_Activity.class));
                        finish();
                    }
                });
                // dialog.setNegativeButton()
                dialog.setCancelable(true);
                dialog.create().show();


            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
//        // dialog.show();
//        dialog.setTitle("");
//        dialog.setMessage("Choose Your Login\n ");
//        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                // startActivity(new Intent(MainActivity.this, Login_Activity.class));
//                finish();
//
//            }
//        });
//        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//
//            }
//        });
//        // dialog.setNegativeButton()
//        dialog.setCancelable(true);
//        dialog.create().show();

    }
}


