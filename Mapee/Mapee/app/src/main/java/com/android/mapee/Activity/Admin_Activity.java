package com.android.mapee.Activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.mapee.Login_Activity;
import com.android.mapee.MainActivity;
import com.android.mapee.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class Admin_Activity extends AppCompatActivity {

    Button user_Login, Admin_Login,
            NotUserPleaseSignUp;

    EditText Admin_MobileNumber,
            Admin_Password;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_);

        user_Login = findViewById(R.id.admin_loginbtn);
        Admin_Login = findViewById(R.id.btn_login);
        NotUserPleaseSignUp = findViewById(R.id.admin_notmember);
        Admin_MobileNumber = findViewById(R.id.Amin_username);
        Admin_Password = findViewById(R.id.Admin_password);

        user_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mAdmin_Mobile = Admin_MobileNumber.getText().toString();
                String mAdmin_Password = Admin_Password.getText().toString();
                if (!(mAdmin_Mobile.isEmpty() || mAdmin_Password.isEmpty())) {
                    String Url = "http://18.216.51.196:8080/arbitorNetwork/user/login?mobileOrMapeeId=" + mAdmin_Mobile + "&password=" + mAdmin_Password + "";
                    RequestQueue requestQueue = Volley.newRequestQueue(Admin_Activity.this);
                    StringRequest request = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.v("", "" + response);
                            if (response.contains("Entity Retrieved Successfully")) {
                                Toast.makeText(Admin_Activity.this, "Successfully", Toast.LENGTH_SHORT).show();
                            } else {
                               /* AlertDialog.Builder dialog = new AlertDialog.Builder(Admin_Activity.this);
                                // dialog.show();
                                dialog.setTitle("Warning !!");
                                dialog.setMessage("Mobile oR Password \ndidn't match !!\n ");
                                dialog.setPositiveButton("SignUp", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        startActivity(new Intent(Admin_Activity.this, MainActivity.class));
                                        finish();

                                    }
                                });
                                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                });
                                // dialog.setNegativeButton()
                                dialog.setCancelable(true);
                                dialog.create().show();*/
                                Toast.makeText(Admin_Activity.this, "Mobile oR Password didn't match", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }

                    });
                    requestQueue.add(request);
                }
                Admin_MobileNumber.setText("");
                Admin_Password.setText("");

            }
        });
        Admin_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Admin_Activity.this, Login_Activity.class));
                finish();
            }
        });
        NotUserPleaseSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Admin_Activity.this, MainActivity.class));
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AlertDialog.Builder dialog = new AlertDialog.Builder(Admin_Activity.this);
        // dialog.show();
        dialog.setTitle("");
        dialog.setMessage("Choose Your Login\n ");
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // startActivity(new Intent(Login_Activity.this, Login_Activity.class));
                finish();

            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        // dialog.setNegativeButton()
        dialog.setCancelable(true);
        dialog.create().show();


    }
}
