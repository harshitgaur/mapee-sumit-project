package com.android.mapee.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.android.mapee.R;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class HouseOwner_Add_Item extends AppCompatActivity {

    EditText Add_shopName, Add_mobileNo, Add_street, Add_state,
            Add_City, Add_Country, Add_pinCode, Add_YearOfEstablish, Add_ShopNumber,
            Add_tenant, Add_ShopType, Add_Offer, Add_ViewAs, Add_Opendays,
            Add_latitude, Add_Longitude, Add_OpenFromTime, Add_CloseTime,
            Add_AddressApproved;
    Button mButton_SaveData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house_owner__add__item);
        Add_mobileNo = findViewById(R.id.fill_mobileno);
        Add_street = findViewById(R.id.fill_street);
        Add_City = findViewById(R.id.fill_City);
        Add_state = findViewById(R.id.fill_State);
        Add_Country = findViewById(R.id.fill_country);
        Add_pinCode = findViewById(R.id.fill_PinCode);
        Add_tenant = findViewById(R.id.Fill_ShopNumber);
        Add_latitude = findViewById(R.id.Fill_Latitude);
        Add_Longitude = findViewById(R.id.Fill_Longitude);
        Add_OpenFromTime = findViewById(R.id.Fill_OpenTime);
        Add_CloseTime = findViewById(R.id.Fill_CloseTime);
        Add_AddressApproved = findViewById(R.id.Fill_AddressAproved);

        mButton_SaveData = findViewById(R.id.saveData);
        mButton_SaveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    ApiDataFillCall();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void ApiDataFillCall() throws JSONException {
        // RequestQueue requestQueue = Volley.newRequestQueue(HouseOwner_Add_Item.this);
        RequestQueue requestQueue = Volley.newRequestQueue(HouseOwner_Add_Item.this);

        //"http://18.216.51.196:8080/arbitorNetwork/user/";
        JSONObject jsonBody = new JSONObject();
        String URL = "http://18.216.51.196:8080/arbitorNetwork/houseOwner/";
        // final String dateDob = dob.getText().toString() + "T" + "00:00:00";
        jsonBody.put("mobileNo", Add_mobileNo.getText().toString());
        jsonBody.put("street", Add_street.getText().toString());
        jsonBody.put("country", Add_Country.getText().toString());
        jsonBody.put("city", Add_City.getText().toString());
        jsonBody.put("state", Add_state.getText().toString());
        jsonBody.put("pinCode", Add_pinCode.getText().toString());
        jsonBody.put("tenant", Add_tenant.getText().toString());
        jsonBody.put("latitude", Add_latitude.getText().toString());
        jsonBody.put("longitude", Add_Longitude.getText().toString());
        jsonBody.put("openingFromTime", Add_OpenFromTime.getText().toString());
        jsonBody.put("openingToTime", Add_CloseTime.getText().toString());
        jsonBody.put("addressApproved", Add_AddressApproved.getText().toString());

        final String requestBody = jsonBody.toString();

        Add_mobileNo.setText("");
        Add_street.setText("");
        Add_Country.setText("");
        Add_City.setText("");
        Add_state.setText("");
        Add_pinCode.setText("");
        Add_tenant.setText("");
        Add_latitude.setText("");
        Add_Longitude.setText("");
        Add_OpenFromTime.setText("");
        Add_CloseTime.setText("");
        Add_AddressApproved.setText("");

        // Toast.makeText(HouseOwner_Add_Item.this, "Successfully ! ", Toast.LENGTH_SHORT).show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("VOLLEY", "" + response);
                Toast.makeText(HouseOwner_Add_Item.this, "Successfully Saved !!", Toast.LENGTH_SHORT).show();
                //getFragmentManager().beginTransaction().replace(R.id.ltn_contaniner, new Login()).commit();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v("VOLLEY", error.toString());
                Toast.makeText(HouseOwner_Add_Item.this, "" + error.toString(), Toast.LENGTH_SHORT).show();
            }
        })

        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";

            }


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> jsonBody = new HashMap<String, String>();

                jsonBody.put("mobileNo", Add_mobileNo.getText().toString());
                jsonBody.put("street", Add_street.getText().toString());
                jsonBody.put("country", Add_Country.getText().toString());
                jsonBody.put("city", Add_City.getText().toString());
                jsonBody.put("state", Add_state.getText().toString());
                jsonBody.put("pinCode", Add_pinCode.getText().toString());
                jsonBody.put("tenant", Add_tenant.getText().toString());
                jsonBody.put("latitude", Add_latitude.getText().toString());
                jsonBody.put("longitude", Add_Longitude.getText().toString());
                jsonBody.put("FromTime", Add_OpenFromTime.getText().toString());
                jsonBody.put("ToTime", Add_CloseTime.getText().toString());
                jsonBody.put("addressApproved", Add_AddressApproved.getText().toString());

                return super.getParams();
            }


            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");

                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            private final Response.Listener<String> onPostsLoaded = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.contentEquals(response.toString())) {
                        Toast.makeText(HouseOwner_Add_Item.this, "toast", Toast.LENGTH_LONG).show();
                        // openProfile();

                    } else {
                        Toast.makeText(HouseOwner_Add_Item.this, response, Toast.LENGTH_LONG).show();
                    }

                }
            };


        };

        requestQueue.add(stringRequest);
    }

}
