package ashish.com.harshit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class Main2Activity extends AppCompatActivity {

    EditText editText1, editText2;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        editText1 = findViewById(R.id.user);
        editText2 = findViewById(R.id.pasa);
        button = findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editText1.getText().toString();
                String pass = editText2.getText().toString();

                String url = "http://14.102.112.174:99/mobileservice.asmx/ValidateUserLogin?EmailId=" + name + "&Password=" + pass + "";
                final RequestQueue requestQueue = Volley.newRequestQueue(Main2Activity.this);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.contains("ServiceEngineer")) {
                            Log.v("gffggh",response);
                            Toast.makeText(Main2Activity.this, "" + response, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(Main2Activity.this, "nhi chla", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Main2Activity.this, "" + error.toString(), Toast.LENGTH_SHORT).show();

                    }
                });
                requestQueue.add(stringRequest);
            }
        });

    }

}
