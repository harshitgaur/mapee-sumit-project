package com.android.retrofitexample;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by arihant on 30/12/17.
 */

public interface Api {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("/user/")
    Call<ResponseBody> postUser(@Body RequestBody requestBody);
}
